<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <parent>
        <artifactId>SpringMVC_Parent</artifactId>
        <groupId>learnByPractice</groupId>
        <version>1.0.1-SNAPSHOT</version>
    </parent>
    
    <modelVersion>4.0.0</modelVersion>
    
    <artifactId>SpringMVC-integrationTests</artifactId>

    <name>Spring MVC byPractice - Integration Tests</name>

    <!--Maven Properties-->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <org-springframework>4.3.3.RELEASE</org-springframework>
        <javax-javaee-version>7.0</javax-javaee-version>
        <h2-version>1.4.178</h2-version>
        <jstl-version>1.2</jstl-version>
        <hibernate.version>4.3.10.Final</hibernate.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <!--Serenity Properties-->
        <serenity.version>1.1.42</serenity.version>
        <serenity.maven.version>1.1.42</serenity.maven.version>
        <webdriver.driver>htmlunit</webdriver.driver>
    </properties>

    <!--Dependency Management-->
    <dependencies>
        
        <!-- Java Dependencies -->
        <dependency>
            <groupId>javax</groupId>
            <artifactId>javaee-web-api</artifactId>
            <version>${javax-javaee-version}</version>
            <scope>provided</scope>
        </dependency>
        
        <!-- Test Dependencies -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>${h2-version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        
        <!--Serenity Dependencies-->
        <dependency>
            <groupId>net.serenity-bdd</groupId>     
            <artifactId>serenity-core</artifactId>
            <version>${serenity.version}</version>
        </dependency>
        <dependency>
            <groupId>net.serenity-bdd</groupId>     
            <artifactId>serenity-junit</artifactId>
            <version>${serenity.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.6.1</version>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>1.7.0</version>
            <scope>test</scope>
        </dependency>
        
    </dependencies>
    
    <build>
        <plugins>
            
            <!--Apache Maven Compiler Plugin-->
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            
            <!--Maven Failsafe Plugin : Integration Tests-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>2.15</version>
                <executions>
                    <!--
                        Ensures that both integration-test and verify goals of the Failsafe Maven
                        plugin are executed.
                    -->
                    <execution>
                        <id>integration-tests</id>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                        <configuration>
                            <!-- Sets the VM argument line used when integration tests are run. -->
                            <argLine>${failsafeArgLine}</argLine>
                            <systemProperties>
                                <webdriver.driver>${webdriver.driver}</webdriver.driver> 
                                <surefire.rerunFailingTestsCount>${surefire.rerunFailingTestsCount}</surefire.rerunFailingTestsCount>
                                <surefire.rerunFailingTestsCount>${surefire.rerunFailingTestsCount}</surefire.rerunFailingTestsCount>
                            </systemProperties>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <!--Serenity PLugin-->
            <plugin>
                <groupId>net.serenity-bdd.maven.plugins</groupId>       
                <artifactId>serenity-maven-plugin</artifactId>
                <version>${serenity.maven.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>net.serenity-bdd</groupId>
                        <artifactId>serenity-core</artifactId>
                        <version>${serenity.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>serenity-reports</id>
                        <phase>post-integration-test</phase>             
                        <goals>
                            <goal>aggregate</goal>                       
                        </goals>
                    </execution>
                </executions>
            </plugin>
            
            <!--JACOCO Unit Test-->
<!--            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.7.5.201505241946</version>
                <executions>
                    
                        Prepares the property pointing to the JaCoCo runtime agent which
                        is passed as VM argument when Maven the Surefire plugin is executed.
                    
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                            <excludes>
                                <exclude>**com.steadystate*</exclude>
                            </excludes>
                             Sets the path to the file which contains the execution data. 
                            <destFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</destFile>
                            
                                Sets the name of the property containing the settings
                                for JaCoCo runtime agent.
                            
                            <propertyName>surefireArgLine</propertyName>
                        </configuration>
                    </execution>
                    
                        Ensures that the code coverage report for unit tests is created after
                        unit tests have been run.
                    
                    <execution>
                        <id>post-unit-test</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                             Sets the path to the file which contains the execution data. 
                            <dataFile>${project.build.directory}/coverage-reports/jacoco-ut.exec</dataFile>
                             Sets the output directory for the code coverage report. 
                            <outputDirectory>${project.reporting.outputDirectory}/jacoco-ut</outputDirectory>
                        </configuration>
                    </execution>
                     The Executions required by unit tests are omitted. 
                    
                        Prepares the property pointing to the JaCoCo runtime agent which
                        is passed as VM argument when Maven the Failsafe plugin is executed.
                    
                    <execution>
                        <id>pre-integration-test</id>
                        <phase>pre-integration-test</phase>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                        <configuration>
                             Sets the path to the file which contains the execution data. 
                            <destFile>${project.build.directory}/coverage-reports/jacoco-it.exec</destFile>
                            
                                Sets the name of the property containing the settings
                                for JaCoCo runtime agent.
                            
                            <propertyName>failsafeArgLine</propertyName>
                        </configuration>
                    </execution>
                    
                        Ensures that the code coverage report for integration tests after
                        integration tests have been run.
                    
                    <execution>
                        <id>post-integration-test</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                             Sets the path to the file which contains the execution data. 
                            <dataFile>${project.build.directory}/coverage-reports/jacoco-it.exec</dataFile>
                             Sets the output directory for the code coverage report. 
                            <outputDirectory>${project.reporting.outputDirectory}/jacoco-it</outputDirectory>
                        </configuration>
                    </execution>
                    Code Coverage Limit
                    <execution>
                        <id>check</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <rule>
                                    <element>BUNDLE</element>
                                    <limits>
                                        <limit>
                                            <counter>LINE</counter>
                                            <value>COVEREDRATIO</value>
                                            <minimum>0</minimum>
                                        </limit>
                                    </limits>
                                </rule>
                            </rules>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            -->
        </plugins>
    </build>

</project>