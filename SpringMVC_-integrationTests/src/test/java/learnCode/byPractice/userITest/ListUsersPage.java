package learnCode.byPractice.userITest;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("http://localhost:9090/springmvcBypractice/user/list.html")
public class ListUsersPage extends PageObject {

	public boolean hasUserWithUsername(String testUser) {
		return this.findAll(By.className("userName")).stream()
			.anyMatch(s -> s.getValue().equals(testUser));
	}

}
