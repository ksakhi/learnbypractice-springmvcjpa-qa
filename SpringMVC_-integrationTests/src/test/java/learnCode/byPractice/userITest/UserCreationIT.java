package learnCode.byPractice.userITest;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class UserCreationIT {

	@Steps
	AdminSteps adminSteps;

	@Managed
	WebDriver browser;

	@Test
	public void adminShouldSuccessfullyCreateUser() {

		// Given
		adminSteps.admin_goes_to_create_user_page();

		// When
		adminSteps.admin_creates_new_user();

		// Then
		adminSteps.admin_should_find_user_in_list_users_table();
	}
}
