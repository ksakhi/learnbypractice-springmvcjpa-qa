package learnCode.byPractice.userITest;

import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class AdminSteps {
	
	CreateUserPage createUserPage;
	
	ListUsersPage listUsersPage;

	@Step
	void admin_goes_to_create_user_page() {
		createUserPage.open();
	}

	@Step
	void admin_creates_new_user() {
		createUserPage.createNewUserWithUsername("testUser");
	}

	@Step
	void admin_should_find_user_in_list_users_table() {
		Assert.assertTrue(listUsersPage.hasUserWithUsername("testUser"));
	}

}
