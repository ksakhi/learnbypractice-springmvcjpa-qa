package learnCode.byPractice.userITest;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:9090/springmvcBypractice/user/createOrUpdate.html")
public class CreateUserPage extends PageObject {

	public void createNewUserWithUsername(String testUser) {

		$("//*[@id='userName']").typeAndTab(testUser);

		$("//*[@id='firstName']").typeAndTab(testUser);

		$("//*[@id='lastName']").typeAndTab(testUser);

		$("//*[@id='email']").typeAndTab("test@tester.testing");

		$("//*[@id='phoneNumber']").typeAndTab("123-123-1234");

		$("//*[@id='age']").typeAndEnter("1");
	}

}
