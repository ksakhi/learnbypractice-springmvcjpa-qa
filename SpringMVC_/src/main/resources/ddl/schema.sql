-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `userDb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `userDb` ;
-- -----------------------------------------------------
-- Schema userDb
-- -----------------------------------------------------
-- USE `userDb` ;

-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `userDb`.`User` ;

CREATE TABLE IF NOT EXISTS `userDb`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL COMMENT 'username of the user, always unique. A username cannot be assigned to more than one user',
  `firstName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NULL,
  `middleName` VARCHAR(45) NULL,
  `phoneNumber` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `age` INT NULL,
  `status` VARCHAR(45),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `userDb`.`User` (`id` ASC);

CREATE UNIQUE INDEX `username_UNIQUE` ON `userDb`.`User` (`username` ASC);
