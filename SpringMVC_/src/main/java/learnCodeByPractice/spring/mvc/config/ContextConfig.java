package learnCodeByPractice.spring.mvc.config;

import java.sql.SQLException;
import javax.persistence.EntityManagerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by aki
 */
@Configuration
@ComponentScan({"learnCodeByPractice.spring.mvc.service", "learnCodeByPractice.spring.mvc.dao"})
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.uii.dps.uhpsi.dao.repository")
public class ContextConfig {

	protected static final String PACKAGE_TO_SCAN_ENTITIES = "learnCodeByPractice.spring.mvc.entity";

	@Value("classpath:ddl/schema.sql")
	private Resource schemaScript;

	@Bean
	public DataSourceInitializer dataSourceInitializer(final SimpleDriverDataSource dataSource) {
		final DataSourceInitializer initializer = new DataSourceInitializer();
		initializer.setDataSource(dataSource);
		initializer.setDatabasePopulator(databasePopulator());
		return initializer;
	}

	private DatabasePopulator databasePopulator() {
		final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		populator.addScript(schemaScript);
		return populator;
	}

	@Bean(name = "transactionManager")
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
		return jpaTransactionManager;
	}

	@Bean(name = "dataSource")
	public SimpleDriverDataSource simpleDriverDataSource() {
		SimpleDriverDataSource simpleDriverDataSource = new SimpleDriverDataSource();
		simpleDriverDataSource.setDriverClass(org.h2.Driver.class);
		simpleDriverDataSource.setUrl("jdbc:h2:mem:userDb;DB_CLOSE_DELAY=-1;MODE=MySql;TRACE_LEVEL_SYSTEM_OUT=2");
		return simpleDriverDataSource;
	}

	@Bean(name = "jpaVendorAdapter")
	public HibernateJpaVendorAdapter jpaAdapter() {

		HibernateJpaVendorAdapter jpaAdapter = new HibernateJpaVendorAdapter();
		jpaAdapter.setGenerateDdl(true);
		jpaAdapter.setDatabasePlatform("org.hibernate.dialect.MySQL5InnoDBDialect");
		return jpaAdapter;
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	public org.h2.tools.Server h2WebConsonleServer() throws SQLException {
		return org.h2.tools.Server.createWebServer("-web", "-webAllowOthers", "-webDaemon", "-webPort", "8083");
	}

	@Bean(name = "entityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(SimpleDriverDataSource dataSource,
		JpaVendorAdapter jpaVendorAdapter) {

		final LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		managerFactoryBean.setDataSource(dataSource);
		managerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		managerFactoryBean.setPackagesToScan(PACKAGE_TO_SCAN_ENTITIES);
		return managerFactoryBean;
	}
}
