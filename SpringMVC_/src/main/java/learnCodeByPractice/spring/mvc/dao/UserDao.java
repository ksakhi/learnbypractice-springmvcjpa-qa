package learnCodeByPractice.spring.mvc.dao;

import learnCodeByPractice.spring.mvc.entity.User;

import java.util.List;

/**
 * Created by aki
 */
public interface UserDao {

	List<User> getUsers();

	User findByUserName(String userName);

	void save(User user);

	void update(User user);

	void remove(String userName);
}
