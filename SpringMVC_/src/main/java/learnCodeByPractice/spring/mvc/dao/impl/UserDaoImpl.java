package learnCodeByPractice.spring.mvc.dao.impl;

import learnCodeByPractice.spring.mvc.dao.UserDao;
import learnCodeByPractice.spring.mvc.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import learnCodeByPractice.spring.mvc.constants.UserStatusEnum;

/**
 * Created by aki
 */
@Repository(value = "userDao")
public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<User> getUsers() {

		return (List<User>) entityManager.createQuery("SELECT OBJECT(u) FROM User AS u")
			.getResultList();
	}

	@Override
	public User findByUserName(String userName) {
		return (User) entityManager.createQuery("SELECT object(u) from User as u where u.userName = :userName")
			.setParameter("userName", userName)
			.getSingleResult();
	}

	@Override
	public void save(User user) {
		entityManager.persist(user);
	}

	@Override
	public void update(User user) {
		entityManager.merge(user);
	}

	@Override
	public void remove(String userName) {
		entityManager.createQuery("update User AS U SET U.status = :status WHERE U.userName = :userName")
			.setParameter("status", UserStatusEnum.DEPRECATED)
			.setParameter("userName", userName)
			.getSingleResult();
	}

}
