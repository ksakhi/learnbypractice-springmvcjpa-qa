package learnCodeByPractice.spring.mvc.service;

import learnCodeByPractice.spring.mvc.entity.User;

import java.util.Collection;

/**
 * Created by aki
 */
public interface UserService {

	Collection<User> getUsers();

	void save(User user);

	void update(User user);

	void remove(String userName);

	User findByUserName(String userName);
}
