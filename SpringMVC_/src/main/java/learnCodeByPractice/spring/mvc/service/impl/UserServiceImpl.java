package learnCodeByPractice.spring.mvc.service.impl;

import java.time.LocalDateTime;
import learnCodeByPractice.spring.mvc.dao.UserDao;
import learnCodeByPractice.spring.mvc.entity.User;
import learnCodeByPractice.spring.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import learnCodeByPractice.spring.mvc.constants.UserStatusEnum;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by aki
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public Collection<User> getUsers() {
		return userDao.getUsers();
	}

	@Override
	@Transactional(readOnly = true)
	public void save(User user) {
		user.setStatus(UserStatusEnum.ACTIVE);
		user.setCreatedDate(LocalDateTime.now());
		userDao.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public void update(User user) {
		userDao.save(user);
	}

	@Transactional
	@Override
	public void remove(String userName) {
		User user = this.findByUserName(userName);
		user.setStatus(UserStatusEnum.DEPRECATED);
		userDao.update(user);
	}

	@Override
	public User findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}
}
