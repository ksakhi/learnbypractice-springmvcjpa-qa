package learnCodeByPractice.spring.mvc.constants;

/**
 *
 * @author aki
 */
public enum UserStatusEnum {

	ACTIVE("Active"),
	INACTIVE("Inactive"),
	DEPRECATED("Deprecated");

	private final String value;

	private UserStatusEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public String getName() {
		return this.name();
	}

}
