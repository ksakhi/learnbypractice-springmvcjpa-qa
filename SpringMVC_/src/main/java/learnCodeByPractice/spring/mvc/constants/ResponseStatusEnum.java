package learnCodeByPractice.spring.mvc.constants;

/**
 *
 * @author aki
 */
public enum ResponseStatusEnum {

	SUCCESS("Success"),
	INVALID("Invalid"),
	FAIL("Fail");

	private final String value;

	private ResponseStatusEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public String getName() {
		return this.name();
	}
}
