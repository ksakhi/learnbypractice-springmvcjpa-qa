package learnCodeByPractice.spring.mvc.web.formObject;

import learnCodeByPractice.spring.mvc.constants.ResponseStatusEnum;

/**
 *
 * @author aki
 */
public class Response {

	private ResponseStatusEnum status;

	private String message;

	public Response(ResponseStatusEnum status, String message) {
		this.status = status;
		this.message = message;
	}

	public ResponseStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ResponseStatusEnum status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
