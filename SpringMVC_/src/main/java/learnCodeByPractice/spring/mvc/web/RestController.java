package learnCodeByPractice.spring.mvc.web;

import learnCodeByPractice.spring.mvc.constants.ResponseStatusEnum;
import learnCodeByPractice.spring.mvc.service.UserService;
import learnCodeByPractice.spring.mvc.web.formObject.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author aki
 */
@org.springframework.web.bind.annotation.RestController
public class RestController {

	@Autowired
	private UserService userService;

	/**
	 * This endpoint is use to remove or delete an user. This end point only
	 * accepts JSON response, so the endpoint should end with .json. Ex:
	 * /user/delete.json
	 *
	 * @param userName
	 * @return Response : Json resonse object
	 */
	@PostMapping(value = "/user/delete", produces = {"application/json"})
	public Response deleteUser_JsonResponse(@RequestParam String userName) {
		try {
			userService.remove(userName);
			return new Response(ResponseStatusEnum.SUCCESS, "Successfully removed user.");
		} catch (Exception e) {
			return new Response(ResponseStatusEnum.FAIL, "There is an error removing user, please try again later.");
		}
	}

	/**
	 * This method relates to the deleteUser_JsonResponse method above, this
	 * method catches all the non JSON response types for /user/delete
	 * endpoint and sends INVALID response.
	 *
	 * @param userName
	 * @return Response : Json resonse object
	 */
	@PostMapping(value = "/user/delete")
	public Response deleteUser_UndefinedResopnse(@RequestParam String userName) {
		return new Response(ResponseStatusEnum.INVALID, "Currently this endpoint only supports JSON resons, plz use .json extension to get the response in JSON format.");
	}

}
