package learnCodeByPractice.spring.mvc.web;

import javax.validation.Valid;
import learnCodeByPractice.spring.mvc.entity.User;
import learnCodeByPractice.spring.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by aki
 */
@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping(value = "/user/list.html")
	public String showUsers(Model model) {

		model.addAttribute("users", userService.getUsers());

		return "user/list";
	}

	@GetMapping(value = "/user/createOrUpdate.html")
	public String addUserPage(Model model) {
		
		model.addAttribute("user", new User());

		return "user/createOrUpdate";
	}

	@PostMapping(value = "/user/createOrUpdate.html")
	public String addUserPostPage(@Valid @ModelAttribute User user, Errors errors) {

		if (errors.hasErrors()) {
			return "user/createOrUpdate";
		}

		userService.save(user);

		return "redirect:/user/list.html";
	}

	@GetMapping(value = "/user/edit.html")
	public String editUserPage(@RequestParam String userName, Model model) {

		model.addAttribute("user", userService.findByUserName(userName));

		return "user/createOrUpdate";
	}

	@GetMapping(value = "/user/dashboard.html")
	public String showDashboard(Model model) {

		return "user/dashboard";
	}

}
