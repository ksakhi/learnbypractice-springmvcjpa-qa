<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:wrapper>
    
    <div class="page-header">
        <h1>Create or update User</h1>
    </div>

    <div class="container">

        <c:url var="formAction" value="/user/createOrUpdate.html"/>
        <form:form modelAttribute="user" action="${formAction}">

            <div class="form-group">
                <label for="userName">User Name</label>
                <form:input type="text" class="form-control errorField" id="userName" placeholder="User Name" path="userName"/>
                <form:errors path="userName" class="error"/>
            </div>

            <div class="form-group">
                <label for="firstName">First Name</label>
                <form:input type="text" class="form-control errorField" id="firstName" placeholder="First name" path="firstName"/>
                <form:errors path="firstName" class="error"/>
            </div>

            <div class="form-group">
                <label for="lastName">Last Name</label>
                <form:input type="text" class="form-control errorField" id="lastName" placeholder="Last Name" path="lastName"/>
                <form:errors path="lastName" class="error"/>
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <form:input type="text" class="form-control errorField" id="email" placeholder="Email" path="email"/>
                <form:errors path="email" class="error"/>
            </div>

            <div class="form-group">
                <label for="phoneNumber">Phone Number</label>
                <form:input type="text" class="form-control errorField" id="phoneNumber" placeholder="Phone Number" path="phoneNumber"/>
                <form:errors path="phoneNumber" class="error"/>
            </div>

            <div class="form-group">
                <label for="age">Age</label>
                <form:input type="number" class="form-control errorField" id="age" placeholder="Age" path="age"/>
                <form:errors path="age" class="error"/>
                <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>

        </form:form>

    </div>
</t:wrapper>