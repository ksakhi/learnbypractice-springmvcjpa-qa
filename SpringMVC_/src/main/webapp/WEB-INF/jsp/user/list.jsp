<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:wrapper>
    <h2>Users:</h2>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <th>UserName</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>CreatedDate</th>
            <th>Status</th>
            <th class="affix"></th>
            <th class="affix"></th>
            </thead>
            <tbody>
                <c:forEach items="${users}" var="user" varStatus="i">
                    <tr>
                        <td class="userName"><c:out value="${user.userName}"/></td>
                        <td><c:out value="${user.firstName}"/></td>
                        <td><c:out value="${user.lastName}"/></td>
                        <td><c:out value="${user.createdDate}"/></td>
                        <td><c:out value="${user.status.value}"/></td>
                        <td>
                            <a href="<c:url value="/user/edit.html"><c:param name="userName" value="${user.userName}"/></c:url>">
                                <span class="glyphicon glyphicon-edit editUser" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="<c:out value="Edit ${user.userName}"/>" data-name="<c:out value="${user.userName}"/>" ></span>
                            </a>
                        </td>
                        <td>
                            <span class="glyphicon glyphicon-trash deleteUser" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="<c:out value="Delete ${user.userName}"/>" data-name="<c:out value="${user.userName}"/>" ></span>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</t:wrapper>