<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:wrapper>
    <div class="text-center">
        <h1>Dashboard</h1>
        <div>
            <fieldset>
                <legend>Create or Update User</legend>
                <a href="<c:url value="/user/createOrUpdate.html"/>" class="btn btn-primary btn-lg btn-block" role="button">Create or Update User</a>
            </fieldset>
            <br/>
            <hr>
            <br/>
            <fieldset>
                <legend>Show Users</legend>
                <a href="<c:url value="/user/list.html"/>" class="btn btn-primary btn-lg btn-block" role="button">Show Users</a>
            </fieldset>
        </div>
    </div>
</t:wrapper>