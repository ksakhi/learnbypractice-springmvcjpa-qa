$(document).ready(function () {


    $('[data-toggle="tooltip"]').tooltip();

    // --> Edit User
    $(".deleteUser").click(function () {
        var uName = $(this).attr("data-name");

        $.ajax({
            // The URL for the request
            url: "/springmvcBypractice/user/delete.json",
            // The data to send (will be converted to a query string)
            data: {
                userName: uName
            },
            // Whether this is a POST or GET request
            type: "POST",
            // The type of data we expect back
            dataType: "json",
        })
                // Code to run if the request succeeds (is done);
                // The response is passed to the function
                .done(function (json) {
                    if (json.status === "SUCCESS") {
                        alert("Successfully deleted " + uName);
                    } else {
                        alert("Failed to delete user :" + uName + ". " + json.message);
                    }
                })
                // Code to run if the request fails; the raw request and
                // status codes are passed to the function
                .fail(function (xhr, status, errorThrown) {
                    alert("Something is wrong try again later.");
                })
    });
    // <-- Edit User





});